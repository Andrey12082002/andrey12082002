package com.intern.person;

public class people {
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final String age;
    private final String sex;
    private final String dateOfBirth;

    public people(String firstName, String lastName, String middleName, String age, String sex, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.age = age;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAge() {
        return age;
    }

    public String getSex() {
        return sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
}
